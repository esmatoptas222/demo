package com.example.application.adapter;

import com.example.demos1.entity.Customer;
import com.fasterxml.jackson.databind.util.JSONPObject;

public class main {
    public static void main(String[] args) {
        RequestAdapter adapter = new RequestAdapter();
        String url = "http://localhost:8080/customer/add";
        Object requestBody = new Customer(14, "Larry", "Banes");
        adapter.sendPostRequest(url, requestBody);
        System.out.println(adapter.sendGetRequestById(1));
    }
}
