package com.example.demos1.service;

import com.example.demos1.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {

    private List<Customer> customerList;

    public CustomerService(){
        customerList = new ArrayList<>();

        Customer customer1 = new Customer(1,"Michael", "Jackson");
        Customer customer2 = new Customer(2, "Mac", "Miller");
        Customer customer3 = new Customer(3, "The", "Weeknd");
        Customer customer4 = new Customer(4, "Whitney", "Houston");

        customerList.addAll(Arrays.asList(customer1,customer2, customer3, customer4));
    }
    public List<Customer> getAll() {
        return customerList;
    }

    public Customer getCustomer(Integer id) {
        for (Customer customer:customerList) {
            if(Objects.equals(customer.getId(), id)){
                return customer;
            }
        }
        return null;
    }

    public Customer setCustomer(Customer customer){
        customer.setId(123);
        this.customerList.add(customer);
        return customer;
    }

    public List<Customer> deleteCustomer(Integer id) {
        this.customerList.remove(getCustomer(id));
        return customerList;
    }
}
