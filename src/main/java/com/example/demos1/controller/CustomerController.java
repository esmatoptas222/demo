package com.example.demos1.controller;


import com.example.demos1.entity.Customer;
import com.example.demos1.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("")
    public List<Customer> index() {
        return customerService.getAll();
    }

    @GetMapping("/getCustomer")
    public Customer getUser (@RequestParam Integer id) {
        return customerService.getCustomer(id);
    }

    @PostMapping("/add")
    public Customer addUser (@RequestBody Customer customer) {
        return customerService.setCustomer(customer);
    }

    @DeleteMapping("/deleteCustomer")
    public void deleteUser (@RequestParam Integer id) {
        customerService.deleteCustomer(id);
    }


}
